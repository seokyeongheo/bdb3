class MeetingsController < ApplicationController

before_action :set_meeting, only: :update

  def create
  	@meeting = Meeting.new(meeting_params)
  	@meeting.start_date = Time.now

  	@meeting.save
  	redirect_to root_path
  end

  def update
  	@meeting.update(meeting_params)
  	redirect_to root_path
  end


private
    # Use callbacks to share common setup or constraints between actions.
    def set_meeting
      @meeting = Meeting.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def meeting_params
      params.require(:meeting).permit(:book_id, :user_id, :start_date, :end_date)
    end

end
