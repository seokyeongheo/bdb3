class Book < ActiveRecord::Base
  belongs_to :user
  has_many :meetings, dependent: :destroy

	acts_as_taggable

  def available?
  	current_meeting.nil?
  end

  def current_meeting
  	meetings.on_date.first
  end

  def late?
  	!available? and (Time.now > (current_meeting.start_date + 2.days))
  end

end
