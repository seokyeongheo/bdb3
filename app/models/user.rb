class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  validates :name, :phone_number, presence: true

	devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :books, dependent: :destroy
  has_many :meetings, dependent: :destroy

  def current_book
    current_meeting.book
  end

  def current_meeting
    meetings.on_date.first
  end

end
