class Meeting < ActiveRecord::Base
  belongs_to :user
  belongs_to :book

  scope :on_date, -> { where(end_date: nil) }
  scope :off, -> { where.not(end_date: nil) }

  def on_date?
  	end_date.nil?
  end

end
